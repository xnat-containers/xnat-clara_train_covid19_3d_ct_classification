#!/usr/bin/env bash
die(){
    echo >&2 "$@"
    echo "Usage: infer.sh config_file_name.json model_file_name.ckpt scan_file_name.nii mask_file_name.nii [additional_options]"    
    exit 1
}
export PYTHONPATH="$PYTHONPATH:/opt/nvidia:/workspace"
export MMAR_ROOT="/workspace"

echo $@

CONFIG_FILE=$1
shift
MODEL_FILE=$1
shift
SCAN=$1
shift
MASK=$1
shift
ADDITIONAL_OPTIONS="$@"

if [ -z $CONFIG_FILE ] || [ -z $MODEL_FILE ] || [ -z $SCAN ] || [ -z $MASK ]; then
	die "Configuration, Model, and Mask file not specified."
else
	CONFIG_FILE=/workspace/config/$CONFIG_FILE
	MODEL_FILE=/workspace/model/$MODEL_FILE
fi

echo "MMAR_ROOT set to $MMAR_ROOT"
echo "Config: $CONFIG_FILE"
echo "ScanFileName: $SCAN"
echo "MaskFileName: $MASK"
echo "ProcessingTask: classification"
echo "MMAR_CKPT: $MODEL_FILE"
echo "Output: /output"
echo "ADDITIONAL_OPTIONS set to $ADDITIONAL_OPTIONS"


echo "Building dataset.json from template and input file."
sed "s/#NIFTI_SCAN#/$SCAN/" /workspace/test_dataset_template.json  > /workspace/dataset.json.part
sed "s/#NIFTI_MASK#/$MASK/" /workspace/dataset.json.part  > /workspace/dataset.json

python3 -u  -m nvmidl.apps.evaluate \
    -m $MMAR_ROOT \
    -c $CONFIG_FILE \
    --set \
    DATA_ROOT=/ \
    DATASET_JSON=/workspace/dataset.json \
    PROCESSING_TASK="classification" \
    MMAR_CKPT=$MODEL_FILE \
    MMAR_CKPT_DIR="/workspace/model" \
    MMAR_EVAL_OUTPUT_PATH="/output" \
    TF_FORCE_GPU_ALLOW_GROWTH=true \
    output_infer_result=true \
    do_validation=false  ${additional_options} \
		|| die "clara evaluate failed"


#echo `find /output/ -type f`
#echo "Moving output files to /output"
#find /output -type f | xargs mv -t /output/ 
#rmdir /output/*


#nvmidl-dataconvert -d /output/ -s .nii.gz -e .nii -o /output/
#rm /output/*.nii.gz

echo "Output files: " `find /output/ `

